﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codin_Game_CSharp
{
    /// <summary>
    /// Helpfull wrapper for letter
    /// </summary>
    class Letter
    {
        public Letter()
        {
            letterLines = new List<string>();
        }

        public List<String> letterLines { get; set; }
        public String getLine(int position)
        {
            return letterLines[position];
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder("");
            foreach (String part in letterLines)
            {
                result.Append(part);
            }

            return result.ToString();
        }
    }

    class Easy_ASCII_Art_Loops_Strings
    {
        static void Main(string[] args)
        {
            int width = int.Parse(Console.ReadLine());
            int height = int.Parse(Console.ReadLine());
            string text = Console.ReadLine().ToUpper();

            // Step 1 - Read all the lines, split each line by width (aka skip + take as width / width-1)
            // add them to a letter dictionary, for later use
            Dictionary<char, Letter> letters = new Dictionary<char, Letter>();
            char currentLetter = 'A';
            for (int i = 0; i < height; i++)
            {
                currentLetter = 'A';
                String ROW = Console.ReadLine();
                char[] letterParts = ROW.ToArray();
                for(int j = 0; j < letterParts.Length; j+=width)
                {
                    String currentLetterPart = new String(letterParts.Skip(j).Take(width).ToArray());

                    if (!letters.ContainsKey(currentLetter))
                        letters.Add(currentLetter, new Letter());

                    letters[currentLetter].letterLines.Add(currentLetterPart);
                    currentLetter++;
                    if (currentLetter > 'Z')
                        currentLetter = '?';
                }
            }

            //Step 2 - Go through the read text and assign the letters
            List<Letter> finalText = new List<Letter>();
            foreach (char letter in text)
            {
                if (letters.ContainsKey(letter))
                    finalText.Add(letters[letter]);
                else
                    finalText.Add(letters['?']);
            }

            //Step 3 - write line by line
            for(int i = 0; i < height; i++)
            {
                foreach (Letter letter in finalText)
                {
                    Console.Write(letter.getLine(i));
                }
                Console.WriteLine();
            }

            // Write an action using Console.WriteLine()
            // To debug: Console.Error.WriteLine("Debug messages...");

            Console.WriteLine("answer");
            
        }
    }
}
