﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

/**
 * The while loop represents the game.
 * Each iteration represents a turn of the game
 * where you are given inputs (the heights of the mountains)
 * and where you have to print an output (the index of the moutain to fire on)
 * The inputs you are given are automatically updated according to your last actions.
 **/
namespace Codin_Game_CSharp
{
    class Easy_The_Descent_Loops_Sorting
    {
        static void Main(string[] args)
        {
            List<Int32> mountainsLoops = new List<Int32>();
            // game loop
            while (true)
            {
                mountainsLoops.Clear();
                for (int i = 0; i < 8; i++)
                {
                    int mountainH = int.Parse(Console.ReadLine()); // represents the height of one mountain.
                    mountainsLoops.Add(mountainH);
                }

                // Write an action using Console.WriteLine()
                // To debug: Console.Error.WriteLine("Debug messages...");
                int index = mountainsLoops.IndexOf(mountainsLoops.Max());

                Console.WriteLine(index); // The index of the mountain to fire on.
            }
        }
    }
}
